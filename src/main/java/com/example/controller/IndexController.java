package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.domain.Fruit;
import com.example.mapper.FruitMapper;

@Controller
public class IndexController {

	@Autowired
	FruitMapper fruitMapper;

	@RequestMapping
	public String index(Model model) {
		List<Fruit> list = fruitMapper.selectAll();
		model.addAttribute("fruits", list);
		return "index";
	}

	@RequestMapping(value = "/insert")
	public String insert(Model model, @RequestParam("name") String name) {

		fruitMapper.insert(name);

		List<Fruit> list = fruitMapper.selectAll();
		model.addAttribute("fruits", list);
		return "index";
	}

	@RequestMapping(value = "/update")
	public String insert(Model model, @RequestParam("chg_before_name") String before_name,
			@RequestParam("chg_after_name") String after_name) {

		fruitMapper.update(before_name, after_name);

		List<Fruit> list = fruitMapper.selectAll();
		model.addAttribute("fruits", list);
		return "index";
	}

	@RequestMapping(value = "/delete")
	public String delete(Model model, @RequestParam("del_name") String name) {

		fruitMapper.delete(name);

		List<Fruit> list = fruitMapper.selectAll();
		model.addAttribute("fruits", list);

		return "index";

	}
}